<?php

namespace App\Traits;

use App\Models\Company;
use App\Models\TeamUser;
use App\Models\CompanyUser;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

trait HasCompanies
{
    public function companies(): HasMany
    {
        return $this->hasMany(Company::class);
    }

    public function currentCompany(): HasOne
    {
        return $this->hasOne(Company::class, 'id', 'current_company_id');
    }

    public function teams(): BelongsToMany
    {
        return $this->belongsToMany(
            Company::class, 'company_user', 'user_id', 'company_id'
        )->using(CompanyUser::class)->withPivot('id');
    }

    public function switchCompanies(Company $company): void
    {
        $this->current_company_id = $company->id;
        $this->save();
    }

    public function assignRolesForCompany(Company $company, ...$roles)
    {
        if($company = $this->teams()->where('companies.id', $company->id)->first()){
            /** @var CompanyUser $companyUser */
            $companyUser = $company->pivot;
            $companyUser->assignRole($roles);
            return;
        }

        throw new Exception('Roles could not be assigned to company user');
    }

    public function can($ability, $arguments = [])
    {
        if(isset($this->current_company_id)){
            /** @var CompanyUser $companyUser */
            $companyUser = $this->teams()->where('companies.id', $this->current_company_id)->first()->pivot;

            if($companyUser->hasPermissionTo($ability)){
                return true;
            }

            // We still run through the gate on fail, as this will check for gate bypass. i.e. Super User
            return app(Gate::class)->forUser($this)->check('InvalidPermission');
        }

        return app(Gate::class)->forUser($this)->check($ability, $arguments);
    }
}