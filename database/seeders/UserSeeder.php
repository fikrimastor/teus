<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Admin users
        $user = \App\Models\User::factory()->create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Company::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Company",
            'user_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'super-admin')->first();
        $team->addTeamMember($user);
        $user->switchCompanies($team);
        // $user->profile()->create();
        // app(\Spatie\Permission\PermissionRegistrar::class)->setPermissionsTeamId($user->current_team_id);
        // $user->assignRole($role);
        // $user->assignRole(['role_id' => $role->id, $user->current_team_id]);
        // $user->syncRoles($role,$team->id);
        // $user->assignRolesForTeam($team, $role);
        // $team->profile()->create();

        // Create Manager User
        $user = \App\Models\User::factory()->create([
            'name' => 'Example Moderator User',
            'email' => 'moderator@example.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Company::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'user_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'manager')->first();
        $team->addTeamMember($user);
        $user->switchCompanies($team);
        // $user->profile()->create();
        // app(\Spatie\Permission\PermissionRegistrar::class)->setPermissionsTeamId($user->current_team_id);
        // $user->assignRole($role);
        // $user->assignRole(['role_id' => $role->id, 'team_id' => $user->current_team_id]);
        // $user->assignRolesForTeam($team, $role);
        // $team->profile()->create();

        // Create member user
        $user = \App\Models\User::factory()->create([
            'name' => 'Example User',
            'email' => 'user@example.com',
            'password' => Hash::make('password'),
        ]);
        
        $team = Company::create([
            'name' => explode(' ', $user->name, 2)[0]."'s Team",
            'user_id' => $user->id,
        ]);
        
        $role = Role::where('name', 'member')->first();
        $team->addTeamMember($user);
        $user->switchCompanies($team);
        // $user->profile()->create();
        // app(\Spatie\Permission\PermissionRegistrar::class)->setPermissionsTeamId($user->current_team_id);
        // $user->assignRole($role);
        // $user->assignRole(['role_id' => $role->id, 'team_id' => $user->current_team_id]);
        // $user->assignRolesForTeam($team, $role);
        // $team->profile()->create();

    }
}
